package v1

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/samandar_tukhtayev/imkon/api/models"
	"gitlab.com/samandar_tukhtayev/imkon/storage/repo"
)

// @Router /accepted-vacancies [post]
// @Summary Create a accepted_vacancy
// @Description Create a accepted_vacancy
// @Tags accepted-vacancies
// @Accept json
// @Produce json
// @Param accepted_vacancy body models.AcceptVacancyReq true "accepted_vacancy"
// @Success 200 {object} models.AcceptVacancyRes
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) AcceptVacancy(c *gin.Context) {
	var (
		req models.AcceptVacancyReq
	)

	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	resp, err := h.storage.AcceptVacancy().AcceptVacancy(&repo.AcceptVacancyReq{
		UserId:    req.UserId,
		VacancyId: req.VacancyId,
	})
	fmt.Println(resp)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, models.AcceptVacancyRes{
		Id: resp.Id,
		UserInfo: models.UserResponse{
			Id:           resp.UserInfo.Id,
			FirstName:    resp.UserInfo.FirstName,
			LastName:     resp.UserInfo.LastName,
			Email:        resp.UserInfo.Email,
			PhoneNumber:  resp.UserInfo.PhoneNumber,
			ImageUrl:     resp.UserInfo.ImageUrl,
			PortfoliaUrl: resp.UserInfo.PortfoliaUrl,
			CreatedAt:    resp.UserInfo.CreatedAt,
		},
		CreatedAt: resp.CreatedAt,
	})
}

// @Router /accepted-vacancies-by-user/{id} [get]
// @Summary Get vacancy by user id
// @Description Get vacancy by user id
// @Tags accepted-vacancies
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.AcceptedVacanciesResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAcceptedVacanciesByUserId(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	accepteds, err := h.storage.AcceptVacancy().GetAcceptedVacanciesByUserId(int64(id))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	var result models.AcceptedVacanciesResponse
	for _, accepted := range accepteds.AcceptedVacancies {
		result.AcceptedVacancies = append(result.AcceptedVacancies, &models.AcceptVacancyRes{
			Id: accepted.Id,
			UserInfo: models.UserResponse{
				Id:           accepted.UserInfo.Id,
				FirstName:    accepted.UserInfo.FirstName,
				LastName:     accepted.UserInfo.LastName,
				Email:        accepted.UserInfo.Email,
				PhoneNumber:  accepted.UserInfo.PhoneNumber,
				ImageUrl:     accepted.UserInfo.ImageUrl,
				PortfoliaUrl: accepted.UserInfo.PortfoliaUrl,
				CreatedAt:    accepted.UserInfo.CreatedAt,
			},
			VacancyInfo: models.Vacancy{
				Id:         accepted.VacancyInfo.Id,
				Name:       accepted.VacancyInfo.Name,
				CategoryId: accepted.VacancyInfo.CategoryId,
				ImageUrl:   accepted.VacancyInfo.ImageUrl,
				Address:    accepted.VacancyInfo.Address,
				JobType:    accepted.VacancyInfo.JobType,
				MinSalary:  accepted.VacancyInfo.MinSalary,
				MaxSalary:  accepted.VacancyInfo.MaxSalary,
				Info:       accepted.VacancyInfo.Info,
				ViewsCount: accepted.VacancyInfo.ViewsCount,
				BusinessId: accepted.VacancyInfo.BusinessId,
				CreatedAt:  accepted.VacancyInfo.CreatedAt,
			},
			CreatedAt: accepted.CreatedAt,
		})
	}
	c.JSON(http.StatusOK, models.AcceptedVacanciesResponse{
		AcceptedVacancies: result.AcceptedVacancies,
		Count:             accepteds.Count,
	})
}

// @Router /accepted-vacancies-by-vacancy/{id} [get]
// @Summary Get vacancy by vacancy id
// @Description Get vacancy by vacancy id
// @Tags accepted-vacancies
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.AcceptedVacanciesResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAcceptedVacanciesByVacancyId(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	accepteds, err := h.storage.AcceptVacancy().GetAcceptedVacanciesByVacancyId(int64(id))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	var result models.AcceptedVacanciesResponse
	for _, accepted := range accepteds.AcceptedVacancies {
		result.AcceptedVacancies = append(result.AcceptedVacancies, &models.AcceptVacancyRes{
			Id: accepted.Id,
			UserInfo: models.UserResponse{
				Id:           accepted.UserInfo.Id,
				FirstName:    accepted.UserInfo.FirstName,
				LastName:     accepted.UserInfo.LastName,
				Email:        accepted.UserInfo.Email,
				PhoneNumber:  accepted.UserInfo.PhoneNumber,
				ImageUrl:     accepted.UserInfo.ImageUrl,
				PortfoliaUrl: accepted.UserInfo.PortfoliaUrl,
				CreatedAt:    accepted.UserInfo.CreatedAt,
			},
			VacancyInfo: models.Vacancy{
				Id:         accepted.VacancyInfo.Id,
				Name:       accepted.VacancyInfo.Name,
				CategoryId: accepted.VacancyInfo.CategoryId,
				ImageUrl:   accepted.VacancyInfo.ImageUrl,
				Address:    accepted.VacancyInfo.Address,
				JobType:    accepted.VacancyInfo.JobType,
				MinSalary:  accepted.VacancyInfo.MinSalary,
				MaxSalary:  accepted.VacancyInfo.MaxSalary,
				Info:       accepted.VacancyInfo.Info,
				ViewsCount: accepted.VacancyInfo.ViewsCount,
				BusinessId: accepted.VacancyInfo.BusinessId,
				CreatedAt:  accepted.VacancyInfo.CreatedAt,
			},
			CreatedAt: accepted.CreatedAt,
		})
	}
	c.JSON(http.StatusOK, models.AcceptedVacanciesResponse{
		AcceptedVacancies: result.AcceptedVacancies,
		Count:             accepteds.Count,
	})
}

// @Router /accepted-vacancies-by-id/{id} [get]
// @Summary Get vacancy by id
// @Description Get vacancy by id
// @Tags accepted-vacancies
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.AcceptVacancyRes
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAcceptedVacanciesById(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	resp, err := h.storage.AcceptVacancy().GetAcceptedVacanciesById(int64(id))
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, models.AcceptVacancyRes{
		Id: resp.Id,
		UserInfo: models.UserResponse{
			Id:           resp.UserInfo.Id,
			FirstName:    resp.UserInfo.FirstName,
			LastName:     resp.UserInfo.LastName,
			Email:        resp.UserInfo.Email,
			PhoneNumber:  resp.UserInfo.PhoneNumber,
			ImageUrl:     resp.UserInfo.ImageUrl,
			PortfoliaUrl: resp.UserInfo.PortfoliaUrl,
			CreatedAt:    resp.UserInfo.CreatedAt,
		},
		VacancyInfo: models.Vacancy{
			Id:         resp.VacancyInfo.Id,
			Name:       resp.VacancyInfo.Name,
			CategoryId: resp.VacancyInfo.CategoryId,
			ImageUrl:   resp.VacancyInfo.ImageUrl,
			Address:    resp.VacancyInfo.Address,
			JobType:    resp.VacancyInfo.JobType,
			MinSalary:  resp.VacancyInfo.MinSalary,
			MaxSalary:  resp.VacancyInfo.MaxSalary,
			Info:       resp.VacancyInfo.Info,
			ViewsCount: resp.VacancyInfo.ViewsCount,
			BusinessId: resp.VacancyInfo.BusinessId,
			CreatedAt:  resp.VacancyInfo.CreatedAt,
		},
		CreatedAt: resp.CreatedAt,
	})
}

// @Summary Delete a accepted_vacancy
// @Description Delete a accepted_vacancy
// @Tags accepted-vacancies
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Failure 500 {object} models.ErrorResponse
// @Router /accepted-vacancy/{id} [delete]
func (h *handlerV1) DeleteAcceptedVacancy(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "failed to convert",
		})
		return
	}

	err = h.storage.AcceptVacancy().DeleteAcceptedVacancy(int64(id))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "successful delete method",
	})
}
