package v1

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/samandar_tukhtayev/imkon/api/models"
	"gitlab.com/samandar_tukhtayev/imkon/storage/repo"
)

// @Router /categories [post]
// @Summary Create a category
// @Description Create a category
// @Tags categories
// @Accept json
// @Produce json
// @Param category body models.CreateCategoryReq true "category"
// @Success 201 {object} models.Category
// @Failure 400 {object} models.ErrorResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) CreateCategory(c *gin.Context) {
	var (
		req models.CreateCategoryReq
	)
	err := c.ShouldBindJSON(&req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	resp, err := h.storage.Category().CreateCategory(&repo.CreateCategoryReq{
		Name: req.Name,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.ErrorResponse{
			Error: err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, models.Category{
		Id:   resp.Id,
		Name: resp.Name,
	})
}

// @Router /categories [get]
// @Summary Get all categories
// @Description Get all categories
// @Tags categories
// @Accept json
// @Produce json
// @Param filter query models.GetAllCategoriesReq false "Filter"
// @Success 200 {object} models.GetAllCategoriesRes
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllCategories(c *gin.Context) {
	req, err := validateGetAllCategoriesParams(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, ErrorResponse(err))
		return
	}

	result, err := h.storage.Category().GetAllCategories(&repo.GetAllCategoriesReq{
		Page:       req.Page,
		Limit:      req.Limit,
		SortByName: req.SortByName,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, ErrorResponse(err))
		return
	}
	c.JSON(http.StatusOK, getCategoriesResponse(result))
}

func getCategoriesResponse(data *repo.GetAllCategoriesRes) *models.GetAllCategoriesRes {
	response := models.GetAllCategoriesRes{
		Categories: make([]*models.Category, 0),
		Count:      data.Count,
	}

	for _, category := range data.Categories {
		categry := &models.Category{
			Id:   category.Id,
			Name: category.Name,
		}
		response.Categories = append(response.Categories, categry)
	}

	return &response
}

func validateGetAllCategoriesParams(c *gin.Context) (*models.GetAllCategoriesReq, error) {
	var (
		limit int = 10
		page  int = 1
		err   error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.Atoi(c.Query("limit"))
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.Atoi(c.Query("page"))
		if err != nil {
			return nil, err
		}
	}

	return &models.GetAllCategoriesReq{
		Limit:      int64(limit),
		Page:       int64(page),
		SortByName: c.Query("sort_by_name"),
	}, nil
}
