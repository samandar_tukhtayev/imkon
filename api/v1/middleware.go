package v1

import (
	"fmt"

	emailPkg "gitlab.com/samandar_tukhtayev/imkon/pkg/email"
	"gitlab.com/samandar_tukhtayev/imkon/pkg/utils"
)

func (h *handlerV1) sendVerificationCode(key, email string) error {
	code, err := utils.GenerateRandomCode(6)
	fmt.Println(code)
	if err != nil {
		return err
	}

	err = h.inMemory.SetWithTTL(key+email, code, 2)
	if err != nil {
		return err
	}

	err = emailPkg.SendEmail(h.cfg, &emailPkg.SendEmailRequest{
		To:      []string{email},
		Subject: "Verification email",
		Body: map[string]string{
			"code": code,
		},
		Type: emailPkg.VerificationEmail,
	})

	if err != nil {
		return err
	}

	return nil
}
