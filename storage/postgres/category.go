package postgres

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/samandar_tukhtayev/imkon/storage/repo"
)

type categoryRepo struct {
	db *sqlx.DB
}

func NewCategory(db *sqlx.DB) repo.CategoryStorageI {
	return &categoryRepo{db: db}
}

func (ctgr *categoryRepo) CreateCategory(req *repo.CreateCategoryReq) (*repo.Category, error) {
	var result repo.Category
	query := `
		INSERT into categories(
			name
		)VALUES($1)
		RETURNING
			id,
			name
	`
	row := ctgr.db.QueryRow(query, req.Name)
	if err := row.Scan(
		&result.Id,
		&result.Name,
	); err != nil {
		return &repo.Category{}, err
	}
	return &result, nil
}

func (ctgr *categoryRepo) GetAllCategories(params *repo.GetAllCategoriesReq) (*repo.GetAllCategoriesRes, error) {
	result := repo.GetAllCategoriesRes{
		Categories: make([]*repo.Category, 0),
	}
	offset := (params.Page - 1) * params.Limit
	if params.SortByName == "" {
		params.SortByName = "desc"
	}
	limit := fmt.Sprintf(" LIMIT %d OFFSET %d ", params.Limit, offset)
	filter := " WHERE true "
	query := `
	SELECT
		id,
		name
	FROM categories ` +
		filter + ` order by name ` +
		params.SortByName + ` ` + limit
	rows, err := ctgr.db.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var category repo.Category
		if err := rows.Scan(
			&category.Id,
			&category.Name,
		); err != nil {
			return &repo.GetAllCategoriesRes{}, err
		}
		result.Categories = append(result.Categories, &category)
	}
	queryCount := `SELECT count(1) FROM categories ` + filter
	err = ctgr.db.QueryRow(queryCount).Scan(&result.Count)
	if err != nil {
		return nil, err
	}

	return &result, nil
}
