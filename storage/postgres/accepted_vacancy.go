package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/samandar_tukhtayev/imkon/storage/repo"
)

type AcceptedVacancyRepo struct {
	db *sqlx.DB
}

func NewAcceptedVacancy(db *sqlx.DB) repo.AcceptedVacancyStorageI {
	return &AcceptedVacancyRepo{db: db}
}

func (acv *AcceptedVacancyRepo) GetUserPortfolia(req int64) string {
	query := `
		SELECT 
			COALESCE(portfolia_url,'') as portfolia_url,
		from users
		WHERE id=$1
	`
	var PortfoliaUrl string
	row := acv.db.QueryRow(query, req)
	if err := row.Scan(&PortfoliaUrl); err != nil {
		return ""
	}
	return PortfoliaUrl
}

func (acv *AcceptedVacancyRepo) AcceptVacancy(req *repo.AcceptVacancyReq) (*repo.AcceptVacancyRes, error) {
	var AcceptedVacancyId int64
	if acv.GetUserPortfolia(req.UserId) == "" {
		return &repo.AcceptVacancyRes{}, errors.New("please, before create your portfolia")
	}

	query := `
		INSERT into accepted_vacancies(
			user_id,
			vacancy_id
		)VALUES($1,$2)
		RETURNING 
			id
	`

	row := acv.db.QueryRow(query, req.UserId, req.VacancyId)
	if err := row.Scan(&AcceptedVacancyId); err != nil {
		return &repo.AcceptVacancyRes{}, err
	}

	return acv.GetAcceptedVacanciesById(AcceptedVacancyId)
}

func (acv *AcceptedVacancyRepo) GetAcceptedVacanciesById(Id int64) (*repo.AcceptVacancyRes, error) {
	var result repo.AcceptVacancyRes
	query := `
		SELECT 
			av.id,
			u.id,
			COALESCE(u.first_name,'') as first_name,
			COALESCE(u.last_name,'') as last_name,
			u.email,
			COALESCE(u.phone_number,'') as phone_number,
			u.password,
			COALESCE(u.image_url,'') as image_url,
			COALESCE(u.portfolia_url,'') as portfolia_url,
			u.created_at,
			v.id,
			COALESCE(v.name,'') as name,
			v.category_id,
			COALESCE(v.image_url,'') as image_url,
			COALESCE(v.address,'') as address,
			v.job_type,
			v.info,
			COALESCE(v.min_salary,0) as min_salary,
			COALESCE(v.max_salary,0) as max_salary,
			v.business_id,
			COALESCE(v.views_count,1) as views_count,
			v.created_at,
			av.created_at
		FROM accepted_vacancies av
		INNER JOIN users u
			ON av.user_id=u.id
		INNER JOIN vacancies v
			ON av.vacancy_id=v.id
		WHERE av.id=$1
	`
	row := acv.db.QueryRow(query, Id)
	if err := row.Scan(
		&result.Id,
		&result.UserInfo.Id,
		&result.UserInfo.FirstName,
		&result.UserInfo.LastName,
		&result.UserInfo.Email,
		&result.UserInfo.PhoneNumber,
		&result.UserInfo.Password,
		&result.UserInfo.ImageUrl,
		&result.UserInfo.PortfoliaUrl,
		&result.UserInfo.CreatedAt,
		&result.VacancyInfo.Id,
		&result.VacancyInfo.Name,
		&result.VacancyInfo.CategoryId,
		&result.VacancyInfo.ImageUrl,
		&result.VacancyInfo.Address,
		&result.VacancyInfo.JobType,
		&result.VacancyInfo.Info,
		&result.VacancyInfo.MinSalary,
		&result.VacancyInfo.MaxSalary,
		&result.VacancyInfo.BusinessId,
		&result.VacancyInfo.ViewsCount,
		&result.VacancyInfo.CreatedAt,
		&result.CreatedAt,
	); err != nil {
		return &repo.AcceptVacancyRes{}, err
	}
	return &result, nil
}

func (acv *AcceptedVacancyRepo) GetAcceptedVacanciesByUserId(Id int64) (*repo.AcceptedVacanciesResponse, error) {
	var result repo.AcceptedVacanciesResponse
	query := `
		SELECT 
			av.id,
			u.id,
			COALESCE(u.first_name,'') as first_name,
			COALESCE(u.last_name,'') as last_name,
			u.email,
			COALESCE(u.phone_number,'') as phone_number,
			u.password,
			COALESCE(u.image_url,'') as image_url,
			COALESCE(u.portfolia_url,'') as portfolia_url,
			u.created_at,
			v.id,
			COALESCE(v.name,'') as name,
			v.category_id,
			COALESCE(v.image_url,'') as image_url,
			COALESCE(v.address,'') as address,
			v.job_type,
			v.info,
			COALESCE(v.min_salary,0) as min_salary,
			COALESCE(v.max_salary,0) as max_salary,
			v.business_id,
			COALESCE(v.views_count,1) as views_count,
			v.created_at,
			av.created_at
		FROM accepted_vacancies av
		INNER JOIN users u
			ON $1=u.id
		INNER JOIN vacancies v
			ON av.vacancy_id=v.id
		WHERE av.user_id=$1
	`
	fmt.Println(query)
	rows, err := acv.db.Query(query, Id)
	if err != nil {
		return &repo.AcceptedVacanciesResponse{}, err
	}
	rows.Close()
	for rows.Next() {
		var response repo.AcceptVacancyRes
		if err := rows.Scan(
			&response.Id,
			&response.UserInfo.Id,
			&response.UserInfo.FirstName,
			&response.UserInfo.LastName,
			&response.UserInfo.Email,
			&response.UserInfo.PhoneNumber,
			&response.UserInfo.Password,
			&response.UserInfo.ImageUrl,
			&response.UserInfo.PortfoliaUrl,
			&response.UserInfo.CreatedAt,
			&response.VacancyInfo.Id,
			&response.VacancyInfo.Name,
			&response.VacancyInfo.CategoryId,
			&response.VacancyInfo.ImageUrl,
			&response.VacancyInfo.Address,
			&response.VacancyInfo.JobType,
			&response.VacancyInfo.Info,
			&response.VacancyInfo.MinSalary,
			&response.VacancyInfo.MaxSalary,
			&response.VacancyInfo.BusinessId,
			&response.VacancyInfo.ViewsCount,
			&response.VacancyInfo.CreatedAt,
			&response.CreatedAt,
		); err != nil {
			return &repo.AcceptedVacanciesResponse{}, err
		}
		result.AcceptedVacancies = append(result.AcceptedVacancies, &response)
		result.Count++
	}
	return &result, nil
}

func (acv *AcceptedVacancyRepo) GetAcceptedVacancies() (*repo.AcceptedVacanciesResponse, error) {
	var result repo.AcceptedVacanciesResponse
	query := `
		SELECT 
			av.id,
			u.id,
			COALESCE(u.first_name,'') as first_name,
			COALESCE(u.last_name,'') as last_name,
			u.email,
			COALESCE(u.phone_number,'') as phone_number,
			u.password,
			COALESCE(u.image_url,'') as image_url,
			COALESCE(u.portfolia_url,'') as portfolia_url,
			u.created_at,
			v.id,
			COALESCE(v.name,'') as name,
			v.category_id,
			COALESCE(v.image_url,'') as image_url,
			COALESCE(v.address,'') as address,
			v.job_type,
			v.info,
			COALESCE(v.min_salary,0) as min_salary,
			COALESCE(v.max_salary,0) as max_salary,
			v.business_id,
			COALESCE(v.views_count,1) as views_count,
			v.created_at,
			av.created_at
		FROM accepted_vacancies av
		INNER JOIN users u
			ON $1=u.id
		INNER JOIN vacancies v
			ON av.vacancy_id=v.id
	`
	fmt.Println(query)
	rows, err := acv.db.Query(query)
	if err != nil {
		return &repo.AcceptedVacanciesResponse{}, err
	}
	rows.Close()
	for rows.Next() {
		var response repo.AcceptVacancyRes
		if err := rows.Scan(
			&response.Id,
			&response.UserInfo.Id,
			&response.UserInfo.FirstName,
			&response.UserInfo.LastName,
			&response.UserInfo.Email,
			&response.UserInfo.PhoneNumber,
			&response.UserInfo.Password,
			&response.UserInfo.ImageUrl,
			&response.UserInfo.PortfoliaUrl,
			&response.UserInfo.CreatedAt,
			&response.VacancyInfo.Id,
			&response.VacancyInfo.Name,
			&response.VacancyInfo.CategoryId,
			&response.VacancyInfo.ImageUrl,
			&response.VacancyInfo.Address,
			&response.VacancyInfo.JobType,
			&response.VacancyInfo.Info,
			&response.VacancyInfo.MinSalary,
			&response.VacancyInfo.MaxSalary,
			&response.VacancyInfo.BusinessId,
			&response.VacancyInfo.ViewsCount,
			&response.VacancyInfo.CreatedAt,
			&response.CreatedAt,
		); err != nil {
			return &repo.AcceptedVacanciesResponse{}, err
		}
		result.AcceptedVacancies = append(result.AcceptedVacancies, &response)
		result.Count++
	}
	return &result, nil
}

func (acv *AcceptedVacancyRepo) GetAcceptedVacanciesByVacancyId(Id int64) (*repo.AcceptedVacanciesResponse, error) {
	var result repo.AcceptedVacanciesResponse
	query := `
		SELECT 
			av.id,
			u.id,
			COALESCE(u.first_name,'') as first_name,
			COALESCE(u.last_name,'') as last_name,
			u.email,
			COALESCE(u.phone_number,'') as phone_number,
			u.password,
			COALESCE(u.image_url,'') as image_url,
			COALESCE(u.portfolia_url,'') as portfolia_url,
			u.created_at,
			v.id,
			COALESCE(v.name,'') as name,
			v.category_id,
			COALESCE(v.image_url,'') as image_url,
			COALESCE(v.address,'') as address,
			v.job_type,
			v.info,
			COALESCE(v.min_salary,0) as min_salary,
			COALESCE(v.max_salary,0) as max_salary,
			v.business_id,
			COALESCE(v.views_count,1) as views_count,
			v.created_at,
			av.created_at
		FROM accepted_vacancies av
		INNER JOIN users u
			ON av.user_id=u.id
		INNER JOIN vacancies v
			ON $1=v.id
		WHERE av.vacancy_id=$1
	`
	rows, err := acv.db.Query(query, Id)
	if err != nil {
		return &repo.AcceptedVacanciesResponse{}, err
	}
	rows.Close()
	for rows.Next() {
		var response repo.AcceptVacancyRes
		if err := rows.Scan(
			&response.Id,
			&response.UserInfo.Id,
			&response.UserInfo.FirstName,
			&response.UserInfo.LastName,
			&response.UserInfo.Email,
			&response.UserInfo.PhoneNumber,
			&response.UserInfo.Password,
			&response.UserInfo.ImageUrl,
			&response.UserInfo.PortfoliaUrl,
			&response.UserInfo.CreatedAt,
			&response.VacancyInfo.Id,
			&response.VacancyInfo.Name,
			&response.VacancyInfo.CategoryId,
			&response.VacancyInfo.ImageUrl,
			&response.VacancyInfo.Address,
			&response.VacancyInfo.JobType,
			&response.VacancyInfo.Info,
			&response.VacancyInfo.MinSalary,
			&response.VacancyInfo.MaxSalary,
			&response.VacancyInfo.BusinessId,
			&response.VacancyInfo.ViewsCount,
			&response.VacancyInfo.CreatedAt,
			&response.CreatedAt,
		); err != nil {
			return &repo.AcceptedVacanciesResponse{}, err
		}
		result.AcceptedVacancies = append(result.AcceptedVacancies, &response)
		result.Count++
	}
	return &result, nil
}
func (acv *AcceptedVacancyRepo) DeleteAcceptedVacancy(Id int64) error {
	effect, err := acv.db.Exec("delete from accepted_vacancies where id=$1", Id)
	if err != nil {
		return err
	}
	rows, err := effect.RowsAffected()
	if err != nil {
		return err
	}
	if rows == 0 {
		return sql.ErrNoRows
	}
	return nil
}
